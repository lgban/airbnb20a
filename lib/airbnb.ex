defmodule Airbnb do
  def ccm, do: %Geo.Polygon{coordinates: [[{19.284829, -99.1697213}]]}
  def csf, do: %Geo.Polygon{coordinates: [[{19.3592502,-99.2609819}]]}
  def example1 do
    %Geo.Polygon{coordinates: [[
      {19.27947808173419, -99.1584565661355},
      {19.29048143661357, -99.14978149565974},
      {19.302986793558276, -99.15537799284813},
      {19.30448825987522, -99.16965199540661},
      {19.29348321909907, -99.17832713620781},
      {19.28097839809594, -99.17272820469735}
    ]]}
  end

  def find_stream(area) do
    File.stream!("listings.csv")
    |> CSV.decode!(headers: true)
    |> Stream.map(fn row -> 
      latitude = row["latitude"] |> String.to_float
      longitude = row["longitude"] |> String.to_float
      %{ 
        id: row["id"], 
        accommodates: row["accommodates"] |> String.to_integer,
        location: %Geo.Polygon{coordinates: [[{latitude, longitude}]]}
      } end)
    |> Stream.filter(fn property ->
      Topo.contains? area, property.location
    end)
    |> Enum.sort_by(fn property -> property.accommodates end, :desc)
    |> Enum.take(10)
  end

  def find_flow(area) do
    File.stream!("listings.csv")
    |> CSV.decode!(headers: true)
    |> Flow.from_enumerable()
    |> Flow.partition()
    |> Flow.map(fn row -> 
      latitude = row["latitude"] |> String.to_float
      longitude = row["longitude"] |> String.to_float
      %{ 
        id: row["id"], 
        accommodates: row["accommodates"] |> String.to_integer,
        location: %Geo.Polygon{coordinates: [[{latitude, longitude}]]}
      } end)
    |> Flow.filter(fn property ->
      Topo.contains? area, property.location
    end)
    |> Enum.sort_by(fn property -> property.accommodates end, :desc)
    |> Enum.take(10)
  end
end